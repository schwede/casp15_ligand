# CASP15 Ligand Scoring

This repository contains code to score CASP ligand predictions.

## Software Requirements

- Python3 (used v. 3.6.6)
- OpenBabel (including Python bindings, used v. 3.1.1)
- OpenStructure (>= 2.4)
- Spyrmsd: https://github.com/RMeli/spyrmsd >= 0.5.2
- NetworkX (used v. 2.5.1)
- Numpy (used v. 1.19.5)

## Data Requirements

This analysis requires CASP data:

- A `targets` folder containing:

  - The ligand target structures
  - The target sequences and ligand SMILES description files
  - Ligand description files derived from the SMILES files above (such as `H1114.ligands.txt`, and included in the `targets` folder)

- A `predictions` folder with the predictions from https://predictioncenter.org/download_area/CASP15/predictions/ligands/, unzipped into their own folders (must be the exact target name)


## Manual interventions

- SMILES for 2MG were corrected to [Mg+2] in `targets`/T1158v4.smiles.txt
- SMILES for ZN and CA were corrected to [Zn+2] and [Ca+2] in `targets`/T1181.smiles.txt
- NAG in T1152 and T1187, OAA in T1181, and LIG in T1186 were merged in the target structures to reflect the molecules in the submissions.
- Ligands with compound IDs not matching the PDB component dictionary were renamed to ___ to avoid OpenStructure assigning incorrect bonds (T1152, T1181, T1186 and T1187)

## parse_predictions.py

This script parses predictions and validates them against the target.

### Output: 

On standard output, a report of the ligands that could be read (ligand-level, only successes).

On standard error stream, extra information about failures.

The following grep command will extract a report with all the problematic prediction files (prediction-level):

    grep "\(Parse\|Validation\) error"

The following grep command will extract a report with all the successful prediction files (prediction-level):

    grep "parsed and validated successfully"

### Example:

    ost parse_predictions.py -d targets/ -p predictions/ -v DEBUG


## score_predictions.py

This script scores the predictions and prints the output on the standard output.

### Output: 

On standard output, a CSV formatted output of the results, with the following columns:

- `target_id`: the Target ID
- `submission_file`: the name of the file containing the predictions
- `model_num`: the MODEL number
- `pose_num`: the POSE number
- `ref_lig`: description of the reference ligand
- `ref_lig_num`: number of the reference ligand
- `ref_lig_compound`: compound ID of the reference ligand
- `ref_lig_formula`: chemical formula of the reference ligand
- `ref_lig_formula_simplified`: simplified chemical formula of the reference ligand (charge and H removed)
- `mdl_lig`: description of the model ligand
- `mdl_lig_name`: name (compound ID) of the model ligand
- `mdl_lig_formula`: chemical formula of the model ligand
- `mdl_lig_formula_simplified`: simplified chemical formula of the model ligand (charge and H removed)
- `lddt_pli`: lDDT-PLI score result, as used for CASP15 scoring
- `rmsd`: RMSD score result, as used for CASP15 scoring
- `lddt_pli_rmsd`: RMSD corresponding to the best lDDT-PLI - NOTE: may differ from best RMSD. Not reported in CASP15
- `lddt_pli_symmetry`: Symmetry ID that yielded the lDDT-PLI - NOTE: a different symmetry ID may have been used for RMSD
- `lddt_pli_n_contacts`: Number of contacts used to compute lDDT-PLI
- `chain_mapping`: Chain mapping used to compute the lDDT-PLI - NOTE: a different chain mapping may have been used for RMSD
- `lddt_bs`: the lDDT of the Binding Site (lDDT-BS) corresponding to the lDDT-PLI - NOTE: a different binding site may have been used for RMSD
- `bs_bb_rmsd`: the RMSD of the binding site backbone after superposition
- `bs_num_res`: the number of residues in the binding site in the target
- `bs_num_overlap_res`: the number of residues in the binding site also present in the model
- `bs_radius`: the radius used to define the binding site
- `lddt_radius`: the lDDT inclusion radius used for lDDT-PLI
- `lddt_bs_radius`: the lDDT inclusion radius used for lDDT-BS
- `substructure_match`: whether the score reflects match of the model ligand to a full (`False`) or partial (`True`) target.

On standard error stream, information about progress and failures, depending on the verbosity level.

### Example:

    ost parse_predictions.py -d targets/ -p predictions/ # Scores all predictions
    ost parse_predictions.py -d targets/ -p predictions/ -t T1127v2 -v DEBUG # Scores predictions for T1127v2, verbose output

### Validation

Targets H1171v1-2, H1172v1-4, T1158v4 and T1170 fail validation (sequence mismatch) and were run with the 
`--fault-tolerant` argument for CASP.
