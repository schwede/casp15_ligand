#!/bin/sh
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=500M
#SBATCH --qos=6hours
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=xavier.robin@unibas.ch


# This is the SLURM run script
# Call:
#     sbatch slurm.sh <TARGET_ID>

# It saves results in the "results" folder.

SCRIPT_PATH=/scicore/home/schwede/zohixe92/projects/casp15/script
TARGET_ID="$1"
BS_RADIUS=$2
LDDT_RADIUS=$3

# Load environment
. ~/activate-ost-develop # OST 2.4 or more
module load OpenBabel/3.1.1-foss-2018b-Python-3.6.6
. ~/CAMEO/venv/bin/activate # Re-use CAMEO venv with spyrmsd

results_path=results/${BS_RADIUS}/${LDDT_RADIUS}
mkdir -p $results_path


rm -rf ${results_path}/${TARGET_ID}.ok
python $SCRIPT_PATH/score_predictions.py -f -b $BS_RADIUS -l $LDDT_RADIUS -t "$TARGET_ID" -d targets -p predictions -v DEBUG > ${results_path}/${TARGET_ID}.csv 2> ${results_path}/${TARGET_ID}.log
exitcode=$?
echo $exitcode > ${results_path}/${TARGET_ID}.ok
exit $exitcode
