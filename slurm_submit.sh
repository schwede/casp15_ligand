#!/bin/sh

# This script submits targets to SLURM
# Call:
#     sbatch slurm.sh

SCRIPT="$(realpath -s "$0")"
SCRIPT_PATH="$(dirname "$SCRIPT")"

# Parameter scan
BS_RADIUS=$1
LDDT_RADIUS=$2

# Heteros
sbatch --job-name=casp_scoring_H1114_${BS_RADIUS}_${LDDT_RADIUS} --mail-type=END,FAIL --qos=1week $SCRIPT_PATH/slurm.sh H1114 $BS_RADIUS $LDDT_RADIUS # > 6h
sbatch --job-name=casp_scoring_H1135_${BS_RADIUS}_${LDDT_RADIUS} --qos=6hours $SCRIPT_PATH/slurm.sh H1135 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_H1171 $SCRIPT_PATH/slurm.sh H1171
sbatch --job-name=casp_scoring_H1171v1_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1171v1 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_H1171v2_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1171v2 $BS_RADIUS $LDDT_RADIUS # This is H1171
#sbatch --job-name=casp_scoring_H1172 $SCRIPT_PATH/slurm.sh H1172
sbatch --job-name=casp_scoring_H1172v1_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1172v1 $BS_RADIUS $LDDT_RADIUS # This is H1172
sbatch --job-name=casp_scoring_H1172v2_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1172v2 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_H1172v3_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1172v3 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_H1172v4_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh H1172v4 $BS_RADIUS $LDDT_RADIUS # This is H1172

# RNA
sbatch --job-name=casp_scoring_R1117v2_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh R1117v2 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_R1126_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh R1126 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_R1136 $SCRIPT_PATH/slurm.sh R1136
# 'R1117' removed because bad smiles?

# Homo
#sbatch --job-name=casp_scoring_T1105v1 $SCRIPT_PATH/slurm.sh T1105v1
#sbatch --job-name=casp_scoring_T1118 $SCRIPT_PATH/slurm.sh T1118
sbatch --job-name=casp_scoring_T1118v1_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1118v1 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1124_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1124 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_T1127 $SCRIPT_PATH/slurm.sh T1127
sbatch --job-name=casp_scoring_T1127v2_${BS_RADIUS}_${LDDT_RADIUS} --qos=6hours $SCRIPT_PATH/slurm.sh T1127v2 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1146_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1146 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1152_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1152 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_T1152_renamed $SCRIPT_PATH/slurm.sh T1152_renamed $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1158v1_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1158v1 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1158v2_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1158v2 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1158v3_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1158v3 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1158v4_${BS_RADIUS}_${LDDT_RADIUS} --qos=6hours $SCRIPT_PATH/slurm.sh T1158v4 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1170_${BS_RADIUS}_${LDDT_RADIUS} --qos=1day $SCRIPT_PATH/slurm.sh T1170 $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1181_${BS_RADIUS}_${LDDT_RADIUS} --qos=6hours $SCRIPT_PATH/slurm.sh T1181 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_T1181_renamed $SCRIPT_PATH/slurm.sh T1181_renamed $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1186_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1186 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_T1186_renamed $SCRIPT_PATH/slurm.sh T1186_renamed $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1187_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1187 $BS_RADIUS $LDDT_RADIUS
#sbatch --job-name=casp_scoring_T1187_renamed $SCRIPT_PATH/slurm.sh T1187_renamed $BS_RADIUS $LDDT_RADIUS
sbatch --job-name=casp_scoring_T1188_${BS_RADIUS}_${LDDT_RADIUS} --qos=30min $SCRIPT_PATH/slurm.sh T1188 $BS_RADIUS $LDDT_RADIUS
