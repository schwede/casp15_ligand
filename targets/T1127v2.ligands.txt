ID  Name    SMILES  Relevant Chain Resnum SubstructureMatch
001 COA CC(C)(CO[P@](=O)(O)O[P@@](=O)(O)OC[C@H]1O[C@@H](n2cnc3c(N)ncnc32)[C@H](O)[C@@H]1OP(=O)(O)O)[C@@H](O)C(=O)NCCC(=O)NCCS   Yes D 400 No
002 COA CC(C)(CO[P@](=O)(O)O[P@@](=O)(O)OC[C@H]1O[C@@H](n2cnc3c(N)ncnc32)[C@H](O)[C@@H]1OP(=O)(O)O)[C@@H](O)C(=O)NCCC(=O)NCCS   Yes C 401 No
003 EPE O=S(=O)(O)CCN1CCN(CCO)CC1   No A 301 No
004 EPE O=S(=O)(O)CCN1CCN(CCO)CC1   No B 301 No
005 MPD C[C@H](O)CC(C)(C)O  No A 401 No